package com.flictures.flictures;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class PhotoRecyclerViewHolder extends RecyclerView.ViewHolder {
    public TextView title;
    public ImageView icon;

    public PhotoRecyclerViewHolder(View itemView) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.PLItitle);
        icon = (ImageView) itemView.findViewById(R.id.PLIicon);
    }
}
