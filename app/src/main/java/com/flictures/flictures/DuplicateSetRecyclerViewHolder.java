package com.flictures.flictures;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class DuplicateSetRecyclerViewHolder extends RecyclerView.ViewHolder {
    public TextView title;
    public TextView count;
    public RecyclerView recyclerView;
    public View view;

    public DuplicateSetRecyclerViewHolder(View itemView) {
        super(itemView);
        view = itemView;
        title = (TextView) itemView.findViewById(R.id.DSLItitle);
        count = (TextView) itemView.findViewById(R.id.DSLIcount);
        recyclerView = (RecyclerView) itemView.findViewById(R.id.DSLIlistRecycler);
    }

}
