package com.flictures.flictures;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flictures.flictures.model.PhotoItemData;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class PhotoRecyclerAdapter extends RecyclerView.Adapter<PhotoRecyclerViewHolder> {
    private static final String TAG = "PhotoRecyclerAdapter";

    private List<PhotoItemData> mData = new ArrayList<>();

    private Activity mActivity;

    private ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

    public PhotoRecyclerAdapter(Activity activity) {
        // Pass context or other static stuff that will be needed.
        mActivity = activity;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public PhotoRecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(R.layout.photo_list_item, viewGroup, false);
        return new PhotoRecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PhotoRecyclerViewHolder viewHolder, int position) {
        Log.v(TAG, String.format("Filling at %s position", position));
        final PhotoItemData data = mData.get(position);

        viewHolder.title.setText(data.text);

        // Load image, decode it to Bitmap and display Bitmap in ImageView (or any other view
        //  which implements ImageAware interface)
        imageLoader.displayImage(data.thumbnailURL, viewHolder.icon, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                try {
                    FileOutputStream out = mActivity.openFileOutput(data.localFileName, mActivity.MODE_PRIVATE);
                    loadedImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    out.flush();
                    out.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
    }

    public void updateList(List<PhotoItemData> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public void addItem(int position, PhotoItemData data) {
        mData.add(position, data);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mData.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        this.mData.clear();
        notifyDataSetChanged();
    }
}
