package com.flictures.flictures.model;

import com.flictures.flictures.Utils;

public class PhotoSetItemData {
    public String text;
    public String thumbnailURL;
    public String photoSetId;
    public String localFileName;

    public PhotoSetItemData(String text, String thumbnailURL, String photoSetId) {
        this.text = text;
        this.thumbnailURL = thumbnailURL;
        this.photoSetId = photoSetId;
        this.localFileName = "i-" + Utils.md5(photoSetId) + ".jpg";
    }
}
