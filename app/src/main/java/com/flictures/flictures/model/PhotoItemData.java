package com.flictures.flictures.model;

import com.flictures.flictures.Utils;

public class PhotoItemData {
    public String id;
    public String text;
    public String thumbnailURL;
    public String localFileName;

    public PhotoItemData(String id, String text, String thumbnailURL) {
        this.id = id;
        this.text = text;
        this.thumbnailURL = thumbnailURL;
        this.localFileName = "i-" + Utils.md5(id) + ".jpg";
    }
}
