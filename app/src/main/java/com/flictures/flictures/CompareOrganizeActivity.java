package com.flictures.flictures;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.photos.PhotosInterface;
import com.flictures.flictures.flickr.FlickrSingleton;
import com.flictures.flictures.model.PhotoItemData;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.imgproc.Imgproc;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CompareOrganizeActivity extends AppCompatActivity {
    private static final String TAG = "CompareOrganizeActivity";

    private ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance
    private List<PhotoItemData> mData = null;
    private List<List<PhotoItemData>> mdisplayListData = null;
    private static Bitmap bmp, yourSelectedImage, bmpimg1, bmpimg2;


    private RecyclerView mRecyclerView;
    private DuplicateSetRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Button mLoadMoreButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Activity activity = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compare_organize);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.COAfab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Updating Flickr", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
                Log.v(TAG, "Deleting " + mAdapter.getItemsToDelete());
                new DeletePhotosFromFlickrAsyncTask(activity).execute(mAdapter.getItemsToDelete());
            }
        });
        mRecyclerView = (RecyclerView) findViewById(R.id.COAduplicateSetListRecycler);
        // Setting the LayoutManager.
        mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        // Setting the adapter.
        mAdapter = new DuplicateSetRecyclerAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Read from intent extras
        Bundle b = getIntent().getExtras();
        ArrayList<String> photoIdsList = b.getStringArrayList(Constants.BUNDLE_PHOTOID_LIST_KEY);
        ArrayList<String> photoURLsList = b.getStringArrayList(Constants.BUNDLE_PHOTOURL_LIST_KEY);
        ArrayList<String> photoTitlesList = b.getStringArrayList(Constants.BUNDLE_PHOTOTITLE_LIST_KEY);
        if (photoIdsList == null || photoURLsList == null || photoTitlesList == null) {
            Log.v(TAG, "Wrong use of activity");
            finish();
        }

        ArrayList<PhotoItemData> photoItemDatas = new ArrayList<>();

        for (int i = 0; i < photoIdsList.size(); i++) {
            PhotoItemData data = new PhotoItemData(photoIdsList.get(i), photoTitlesList.get(i), photoURLsList.get(i));
            photoItemDatas.add(data);

        }
        mData = photoItemDatas;
        /*
        // Locate and read bitmaps from local storage
        Bitmap bitmap = null;
        for (int i = 0; i < mData.size(); i++) {
            PhotoItemData data = mData.get(i);
            FileInputStream fileInputStream = null;
            try {
                fileInputStream = this.openFileInput(data.localFileName);
                bitmap = BitmapFactory.decodeStream(fileInputStream);
                fileInputStream.close();
            } catch (FileNotFoundException e) {
                Log.v(TAG, "Exception occured", e);
            } catch (IOException e) {
                Log.v(TAG, "Exception occured", e);
            }
        }
        */

        // Load OpenCV library, if it is not loaded yet
        if (!OpenCVLoader.initDebug()) {
            Log.d("OpenCV", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d("OpenCV", "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    private void doCompareOrganize() {
        final Activity activity = this;
//        this.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//
//                // Toast user of no more images exist
//                Context context = activity.getApplicationContext();
//                CharSequence text = "Calculating duplicates.";
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, text, duration);
//                toast.show();
//
//            }
//        });
        //final Map<Integer, Set<Integer>> matchImg = null;
        final Map<Integer, Set<Integer>> matchImg = new HashMap<Integer, Set<Integer>>();
        //Get number of images at hand - nI
        Set<Integer> set;
        int nI = mData.size();
        String filePathI, filePathJ;
        int[] markArray = new int[nI];
        for (int in = 0; in < nI; in++) {
            markArray[in] = -1;
        }
        String[] imageArray = new String[mData.size()];
        //PhotoItemData[] imageArray = new PhotoItemData[][mData.size()];
        List<Mat> histArray = new ArrayList<>();
        FileInputStream fileInputStream = null;
        for (int in = 0; in < nI; in++) {
            imageArray[in] = mData.get(in).localFileName;
            try {
                fileInputStream = this.openFileInput(imageArray[in]);
                bmpimg1 = BitmapFactory.decodeStream(fileInputStream);
                bmpimg1 = Bitmap.createScaledBitmap(bmpimg1, 256, 256, true);
                Mat img1 = new Mat();
                org.opencv.android.Utils.bitmapToMat(bmpimg1, img1);
                Imgproc.cvtColor(img1, img1, Imgproc.COLOR_RGBA2GRAY);
                img1.convertTo(img1, CvType.CV_32F);
                Mat hist1 = new Mat();
                MatOfInt histSize = new MatOfInt(180);
                MatOfInt channels = new MatOfInt(0);
                ArrayList<Mat> bgr_planes1 = new ArrayList<Mat>();
                Core.split(img1, bgr_planes1);
                MatOfFloat histRanges = new MatOfFloat(0f, 180f);
                boolean accumulate = false;

                Imgproc.calcHist(bgr_planes1, channels, new Mat(), hist1, histSize, histRanges, accumulate);
                Core.normalize(hist1, hist1, 0, hist1.rows(), Core.NORM_MINMAX, -1, new Mat());
                img1.convertTo(img1, CvType.CV_32F);
                hist1.convertTo(hist1, CvType.CV_32F);
                histArray.add(hist1);
                fileInputStream.close();
            } catch (FileNotFoundException e) {
                Log.v(TAG, "Exception occured", e);
            } catch (IOException e) {
                Log.v(TAG, "Exception occured", e);
            }

        }

        int in;
        for (in = 0; in < nI; in++) {
            if (markArray[in] > -1) {
                continue;
            }

            for (int j = in + 1; j < nI; j++) {
                if (markArray[j] > -1) {
                    continue;
                }
                Mat hist1 = histArray.get(in), hist2 = histArray.get(j);
                double compare = Imgproc.compareHist(hist1, hist2, Imgproc.CV_COMP_CHISQR);
                int key;
                Log.d("ImageComparator", "compare: " + compare);
                if ((compare > 0 && compare < 1500) || compare == 0) {
                    // Images are same
                    if (markArray[in] != -1) {
                        key = markArray[in];
                    } else {
                        key = in;
                    }
                    set = null;
                    set = matchImg.get(key);
                    if (set == null) {
                        set = new HashSet<Integer>();
                        matchImg.put(key, set);
                        set.add(key);
                        markArray[key] = key;
                    }
                    set.add(j);
                    markArray[j] = key;
                } else {
                    // Images are different
                    Set<Integer> theSet = matchImg.get(in);
                    if (theSet == null) {
                        theSet = new HashSet<Integer>();
                        matchImg.put(in, theSet);
                    }
                    theSet.add(in);
                    markArray[in] = in;
                }

            }

        }
        if (markArray[nI - 1] == -1) {
            markArray[nI - 1] = nI - 1;
            Set<Integer> theSet = matchImg.get(nI - 1);
            if (theSet == null) {
                theSet = new HashSet<Integer>();
                matchImg.put(nI - 1, theSet);
            }
            theSet.add(nI - 1);
        }
        Set<Set<Integer>> finalMerge = new HashSet<Set<Integer>>();
        Set<Integer> curSet = new HashSet<Integer>();
        Set<Integer> curmergeSet;
        in = 0;
        for (in = 0; in < nI; in++) {
            if (markArray[in] == in) {
                curSet = matchImg.get(in);
                curmergeSet = new HashSet<Integer>();
                for (Integer simIndex : curSet) {
                    curmergeSet.add(simIndex);
                }
                finalMerge.add(curmergeSet);
            }
        }
        Log.v(TAG, "Final merge set - " + finalMerge);
        mdisplayListData = new ArrayList<>(finalMerge.size());
        for (Set<Integer> items : finalMerge) {
            if (items.size() < 2) {
                continue;
            }

            List<PhotoItemData> l = new ArrayList<>();
            for (Integer index : items) {
                l.add(mData.get(index));
            }
            mdisplayListData.add(l);
        }

        // Update adapter.
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.updateList(mdisplayListData);
                if (mdisplayListData.size() == 0) {
                    // Toast user of no more images exist
                    Context context = activity.getApplicationContext();
                    CharSequence text = "No duplicates found.";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
        });
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i("OpenCV", "OpenCV loaded successfully");
                    new ComparePhotosAsyncTask().execute();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    private class ComparePhotosAsyncTask extends AsyncTask<URL, Integer, Long> {
        private Activity mActivity;

        protected Long doInBackground(URL... urls) {
            doCompareOrganize();
            return 0L;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Long result) {

        }
    }

    private class DeletePhotosFromFlickrAsyncTask extends AsyncTask<List<PhotoItemData>, Integer, Long> {
        private Activity mActivity;

        DeletePhotosFromFlickrAsyncTask(Activity activity) {
            this.mActivity = activity;
        }

        protected Long doInBackground(List<PhotoItemData>... photoItemDatas) {
            try {

                Flickr f = new Flickr(Constants.apiKey, Constants.sharedSecret, new REST());
                Auth mainAuth = f.getAuthInterface().checkToken(FlickrSingleton.getInstance().getAccessToken().getToken(),
                        FlickrSingleton.getInstance().getAccessToken().getTokenSecret());
                f.setAuth(mainAuth);
                RequestContext.getRequestContext().setAuth(mainAuth);
                PhotosInterface photosInterface = f.getPhotosInterface();

                for (List<PhotoItemData> p : photoItemDatas) {
                    for (PhotoItemData d : p) {
                        photosInterface.delete(d.id);
                        // Make delete item id as null
                        d.id = null;


                        // Update adapter.
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mAdapter.cleanUp();
                                Context context = mActivity.getApplicationContext();
                                CharSequence text = "Deleted photos";
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();

                            }
                        });
                    }
                }
            } catch (Exception e) {
                Log.v(TAG, "Exception occurred.", e);
            }
            return 0L;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Long result) {

        }
    }
}
