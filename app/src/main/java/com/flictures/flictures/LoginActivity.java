package com.flictures.flictures;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.flictures.flictures.flickr.FlickrSingleton;
import com.github.scribejava.apis.FlickrApi;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.oauth.OAuth10aService;

import java.net.URL;

import static com.flictures.flictures.Constants.PREFS_NAME;
import static com.flictures.flictures.Constants.apiKey;
import static com.flictures.flictures.Constants.sharedSecret;

public class LoginActivity extends AppCompatActivity {

    private final OAuth10aService service = new ServiceBuilder()
            .apiKey(apiKey)
            .apiSecret(sharedSecret)
            .build(FlickrApi.instance());

    // Value is set in Async Task
    private OAuth1RequestToken requestToken = null;

    // Value is set in Async Task
    private String oauthVerifier = "";

    // Value is set in Async Task
    private OAuth1AccessToken accessToken = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // FIXME: FloatingActionButton Not required right now. Will add if required later..
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
//

        final Button authorizeButton = (Button) findViewById(R.id.LAauthorizeButton);
        final Activity activity = this;
        authorizeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText editApproverCodeText = (EditText) findViewById(R.id.LAeditApproverCodeText);
                String authorizeCode = editApproverCodeText.getText().toString();
                if (authorizeCode != null)
                    authorizeCode = authorizeCode.replace("-", "");
                oauthVerifier = authorizeCode;
                new GetAuthTokenTask(activity).execute();
            }
        });
        checkAndSetAccessToken();
    }

    private void checkAndSetAccessToken() {
        // Restore preferences
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        String spfAccessToken = settings.getString(Constants.ACCESS_TOKEN_KEY, null);
        String spfAccessTokenSecret = settings.getString(Constants.ACCESS_TOKEN_SECRET_KEY, null);
        if (spfAccessToken == null) {
            new OpenWebViewForUser().execute();
            return;
        }
        else {
            FlickrSingleton.getInstance().setAccessToken(new OAuth1AccessToken(spfAccessToken, spfAccessTokenSecret), this);
            finish();
        }
    }

    private class OpenWebViewForUser extends AsyncTask<URL, Integer, Long> {
        protected Long doInBackground(URL... urls) {
            try {

                System.out.println("=== Flickr's OAuth Workflow ===");
                System.out.println();

                // Obtain the Request Token
                System.out.println("Fetching the Request Token...");
                requestToken = service.getRequestToken();
                System.out.println("Got the Request Token!");
                System.out.println();

                System.out.println("Now go and authorize ScribeJava here:");
                final String authorizationUrl = service.getAuthorizationUrl(requestToken);

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(authorizationUrl + "&perms=delete"));
                startActivity(browserIntent);
            } catch (Exception e) {
                System.out.println(e);
            }
            return 0L;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Long result) {

        }
    }

    private class GetAuthTokenTask extends AsyncTask<String, Integer, String> {
        Activity activity;

        public GetAuthTokenTask(Activity activity) {
            this.activity = activity;
        }

        protected String doInBackground(String... authorizeCode) {
            try {
                System.out.println("And paste the verifier here");
                System.out.print(">>");
                System.out.println();

                // Trade the Request Token and Verfier for the Access Token
                System.out.println("Trading the Request Token for an Access Token...");
                accessToken = service.getAccessToken(requestToken, oauthVerifier);
                System.out.println("Got the Access Token!");
                System.out.println("(if your curious it looks like this: " + accessToken
                        + ", 'rawResponse'='" + accessToken.getRawResponse() + "')");
                System.out.println();
                FlickrSingleton.getInstance().setAccessToken(accessToken, activity);
//                // Now let's go and ask for a protected resource!
//                System.out.println("Now we're going to access a protected resource...");
//                final OAuthRequest request = new OAuthRequest(Verb.GET, PROTECTED_RESOURCE_URL, service);
//                request.addQuerystringParameter("method", "flickr.test.login");
//                service.signRequest(accessToken, request);
//                final Response response = request.send();
//                System.out.println("Got it! Lets see what we found...");
//                System.out.println();
//                System.out.println(response.getBody());

                // We need an Editor object to make preference changes.
                // All objects are from android.context.Context
                SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(Constants.ACCESS_TOKEN_KEY, accessToken.getToken());
                editor.putString(Constants.ACCESS_TOKEN_SECRET_KEY, accessToken.getTokenSecret());
                // Commit the edits!
                editor.commit();

                System.out.println();
                System.out.println("Thats it man! Go and build something awesome with ScribeJava! :)");
            } catch (Exception e) {
                System.out.println(e);
            }
            return "";
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(String result) {
            Context context = activity.getApplicationContext();
            CharSequence text = "Logged in.";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            finish();
        }
    }
}
