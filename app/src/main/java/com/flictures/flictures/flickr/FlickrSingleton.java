package com.flictures.flictures.flickr;

import android.app.Activity;
import android.content.SharedPreferences;

import com.github.scribejava.core.model.OAuth1AccessToken;

import static com.flictures.flictures.Constants.ACCESS_TOKEN_KEY;
import static com.flictures.flictures.Constants.ACCESS_TOKEN_SECRET_KEY;
import static com.flictures.flictures.Constants.PREFS_NAME;

public class FlickrSingleton {
    private static FlickrSingleton singleton = new FlickrSingleton();
    private OAuth1AccessToken accessToken;

    /* A private Constructor prevents any other
     * class from instantiating.
     */
    private FlickrSingleton() {
    }

    /* Static 'instance' method */
    public static FlickrSingleton getInstance() {
        return singleton;
    }

    public boolean isLoggedIn() {
        return singleton.getAccessToken() != null;
    }

    public OAuth1AccessToken getAccessToken() {
        return accessToken;
    }

    /**
     * Set auth token value whenever a change in them is detected.
     * @param accessToken
     * @param activity
     */
    public void setAccessToken(OAuth1AccessToken accessToken, Activity activity) {
        this.accessToken = accessToken;
        if (this.accessToken == null) {
            // We need an Editor object to make preference changes.
            // All objects are from android.context.Context
            SharedPreferences settings = activity.getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(ACCESS_TOKEN_KEY, null);
            editor.putString(ACCESS_TOKEN_SECRET_KEY, null);
            // Commit the edits!
            editor.commit();
        }
    }

}
