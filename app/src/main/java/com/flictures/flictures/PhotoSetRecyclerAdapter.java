package com.flictures.flictures;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flictures.flictures.model.PhotoSetItemData;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.FileOutputStream;
import java.util.Collections;
import java.util.List;

import static com.flictures.flictures.Constants.PHOTOSET_ID_KEY;

public class PhotoSetRecyclerAdapter extends RecyclerView.Adapter<PhotoSetRecyclerViewHolder> {
    private static final String TAG = "PhotoSetRecyclerAdapter";

    private Activity mActivity;
    private List<PhotoSetItemData> mData = Collections.emptyList();
    private ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

    public PhotoSetRecyclerAdapter(Activity activity) {
        // Pass context or other static stuff that will be needed.
        mActivity = activity;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public PhotoSetRecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(R.layout.photo_set_list_item, viewGroup, false);
        return new PhotoSetRecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PhotoSetRecyclerViewHolder viewHolder, int position) {
        Log.v(TAG, String.format("Filling at %s position", position));
        final PhotoSetItemData data = mData.get(position);

        viewHolder.title.setText(data.text);

        // Load image, decode it to Bitmap and display Bitmap in ImageView (or any other view
        //  which implements ImageAware interface)
        imageLoader.displayImage(data.thumbnailURL, viewHolder.icon, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {

            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                try {
                    FileOutputStream out = mActivity.openFileOutput(data.localFileName, mActivity.MODE_PRIVATE);
                    loadedImage.compress(Bitmap.CompressFormat.JPEG, 100, out);
                    out.flush();
                    out.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });

        viewHolder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(mActivity, PhotoListActivity.class);
                intent.putExtra(PHOTOSET_ID_KEY, data.photoSetId);
                // Update adapter.
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.startActivity(intent);
                    }
                });
            }
        });
    }

    public void updateList(List<PhotoSetItemData> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public void addItem(int position, PhotoSetItemData data) {
        mData.add(position, data);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mData.remove(position);
        notifyItemRemoved(position);
    }
}
