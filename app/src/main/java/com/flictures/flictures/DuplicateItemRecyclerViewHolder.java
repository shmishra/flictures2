package com.flictures.flictures;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

public class DuplicateItemRecyclerViewHolder extends RecyclerView.ViewHolder {
    public ImageView icon;
    public View view;

    public DuplicateItemRecyclerViewHolder(View itemView) {
        super(itemView);
        view = itemView;
        icon = (ImageView) itemView.findViewById(R.id.DLIicon);
    }
}
