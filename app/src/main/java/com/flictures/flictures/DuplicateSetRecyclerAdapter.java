package com.flictures.flictures;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flictures.flictures.model.PhotoItemData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class DuplicateSetRecyclerAdapter extends RecyclerView.Adapter<DuplicateSetRecyclerViewHolder> {
    private static final String TAG = "D~SetRecyclerAdapter";

    private Activity mActivity;
    private List<List<PhotoItemData>> mData = Collections.emptyList();
    private ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

    private List<PhotoItemData> itemsToDelete = new ArrayList<>();

    public DuplicateSetRecyclerAdapter(Activity activity) {
        // Pass context or other static stuff that will be needed.
        mActivity = activity;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public DuplicateSetRecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(R.layout.duplicate_set_list_item, viewGroup, false);
        return new DuplicateSetRecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DuplicateSetRecyclerViewHolder viewHolder, int position) {
        Log.v(TAG, String.format("Filling at %s position", position));
        final List<PhotoItemData> list = mData.get(position);

        viewHolder.count.setText(new Integer(list.size()).toString());

        DuplicateItemRecyclerAdapter adapter = new DuplicateItemRecyclerAdapter(mActivity, itemsToDelete);
        viewHolder.recyclerView.setAdapter(adapter);
        viewHolder.recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
        viewHolder.recyclerView.setLayoutManager(layoutManager);
        adapter.updateList(list);
    }

    public void updateList(List<List<PhotoItemData>> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public void addItem(int position, List<PhotoItemData> data) {
        mData.add(position, data);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mData.remove(position);
        notifyItemRemoved(position);
    }

    public void cleanUp() {
        for (Iterator<List<PhotoItemData>> itpdl = mData.iterator(); itpdl.hasNext(); ) {
            List<PhotoItemData> l = itpdl.next();
            for (Iterator<PhotoItemData> itpd = l.iterator(); itpd.hasNext(); ) {
                PhotoItemData p = itpd.next();
                if (p.id == null) {
                    itpd.remove();
                }
            }
            if (l.size() == 0)
                itpdl.remove();
        }
        notifyDataSetChanged();
    }

    public List<PhotoItemData> getItemsToDelete() {
        return itemsToDelete;
    }
}
