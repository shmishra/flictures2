package com.flictures.flictures;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.photosets.Photoset;
import com.flickr4java.flickr.photosets.Photosets;
import com.flickr4java.flickr.photosets.PhotosetsInterface;
import com.flictures.flictures.flickr.FlickrSingleton;
import com.flictures.flictures.model.PhotoSetItemData;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PhotoSetListActivity extends AppCompatActivity {
    private static final String TAG = "PhotoSetListActivity";


    private RecyclerView mRecyclerView;
    private PhotoSetRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private List<PhotoSetItemData> mData = new ArrayList<>();

    private void checkAndUpdateLoginStatus() {
        if (!FlickrSingleton.getInstance().isLoggedIn()) {
            Log.v(TAG, "OpenUp LoginActivity");
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            return;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Activity activity = this;

        // FIXME This could be put in Application class, instead of here
        // Create global configuration and initialize ImageLoader with this config
        DisplayImageOptions options = new DisplayImageOptions.Builder().showImageOnLoading(R.mipmap.ic_loading) // resource or drawable
                .showImageForEmptyUri(R.mipmap.ic_empty_uri) // resource or drawable
                .showImageOnFail(R.mipmap.ic_load_fail) // resource or drawable
                .delayBeforeLoading(200)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).defaultDisplayImageOptions(options)
                .build();
        ImageLoader.getInstance().init(config);

        setContentView(R.layout.activity_photo_set_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Fetching the photo sets again", Snackbar.LENGTH_LONG)
                        .setAction("Reload", null).show();
                new FetchPhotoSetsAsyncTask(activity).execute();
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.GLAphotoSetListRecycler);
        // Setting the LayoutManager.
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        // Setting the adapter.
        mAdapter = new PhotoSetRecyclerAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        // Restore preferences
        SharedPreferences settings = getSharedPreferences(Constants.PREFS_NAME, 0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkAndUpdateLoginStatus();
        if (mData.size() == 0) {
            Activity activity = this;
            new FetchPhotoSetsAsyncTask(activity).execute();
        }
    }

    /**
     * Clean up state of activity
     */
    private void resetModel() {
        mData.clear();
    }

    // Called when to add an item to adapter list
    public void addPhotoSetItem(PhotoSetItemData dataToAdd) {
        mData.add(dataToAdd);
        // Update adapter.
        mAdapter.addItem(mData.size() - 1, dataToAdd);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_photo_set_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_logout) {
            FlickrSingleton.getInstance().setAccessToken(null, this);
            checkAndUpdateLoginStatus();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class FetchPhotoSetsAsyncTask extends AsyncTask<URL, Integer, Long> {
        private Activity mActivity;

        FetchPhotoSetsAsyncTask(Activity activity) {
            mActivity = activity;
        }

        protected Long doInBackground(URL... urls) {
            Log.v(TAG, "Fetch photosets");
            try {
                Flickr f = new Flickr(Constants.apiKey, Constants.sharedSecret, new REST());
                Auth mainAuth = f.getAuthInterface().checkToken(FlickrSingleton.getInstance().getAccessToken().getToken(),
                        FlickrSingleton.getInstance().getAccessToken().getTokenSecret());
                f.setAuth(mainAuth);
                RequestContext.getRequestContext().setAuth(mainAuth);
                PhotosetsInterface photosetsInterface = f.getPhotosetsInterface();
                String userId = f.getAuth().getUser().getId();

                Photosets results = photosetsInterface.getList(userId);
                Log.v(TAG, "index=" + results);
                mData.clear();
                for (Photoset p : results.getPhotosets()) {
                    PhotoSetItemData data = new PhotoSetItemData(p.getTitle(), p.getPrimaryPhoto().getSmall320Url(), p.getId());
                    mData.add(data);
                }
                // Update adapter.
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter.updateList(mData);
                    }
                });
                Log.v(TAG, "Adapter data update complete");
            } catch (Exception e) {
                Log.v(TAG, "Exception occurred.", e);
            }
            return 0L;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Long result) {

        }
    }
}
