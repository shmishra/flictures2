package com.flictures.flictures;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flictures.flictures.model.PhotoItemData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Collections;
import java.util.List;

public class DuplicateItemRecyclerAdapter extends RecyclerView.Adapter<DuplicateItemRecyclerViewHolder> {
    private static final String TAG = "D~SetRecyclerAdapter";

    private Activity mActivity;
    private List<PhotoItemData> mData = Collections.emptyList();
    private List<PhotoItemData> photoItemToDeleteList;
    private ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

    public DuplicateItemRecyclerAdapter(Activity activity, List<PhotoItemData> photoItemToDeleteList) {
        // Pass context or other static stuff that will be needed.
        this.mActivity = activity;
        this.photoItemToDeleteList = photoItemToDeleteList;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public DuplicateItemRecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = inflater.inflate(R.layout.duplicate_list_item, viewGroup, false);
        return new DuplicateItemRecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DuplicateItemRecyclerViewHolder viewHolder, int position) {
        Log.v(TAG, String.format("Filling at %s position", position));
        final PhotoItemData data = mData.get(position);

        // Load image, decode it to Bitmap and display Bitmap in ImageView (or any other view
        //  which implements ImageAware interface)
        imageLoader.displayImage(data.thumbnailURL, viewHolder.icon);
        viewHolder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.icon.getColorFilter()!= null){
                    viewHolder.icon.clearColorFilter();
                    photoItemToDeleteList.remove(data);
                }
                else {
                    viewHolder.icon.setColorFilter(Color.RED);
                    photoItemToDeleteList.add(data);
                }
            }
        });
    }

    public void updateList(List<PhotoItemData> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public void addItem(int position, PhotoItemData data) {
        mData.add(position, data);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        mData.remove(position);
        notifyItemRemoved(position);
    }
}
