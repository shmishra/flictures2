package com.flictures.flictures;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.RequestContext;
import com.flickr4java.flickr.auth.Auth;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photosets.PhotosetsInterface;
import com.flictures.flictures.flickr.FlickrSingleton;
import com.flictures.flictures.model.PhotoItemData;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PhotoListActivity extends AppCompatActivity {
    private static final String TAG = "PhotoListActivity";

    private RecyclerView mRecyclerView;
    private PhotoRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Button mLoadMoreButton;

    private String photosetId;
    private int pageToLoad = 1;
    private boolean canLoadMore = true;

    private List<PhotoItemData> mData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Activity activity = this;

        setContentView(R.layout.activity_photo_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.PLAphotoListRecycler);
        // Setting the LayoutManager.
        mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        // Setting the adapter.
        mAdapter = new PhotoRecyclerAdapter(activity);
        mRecyclerView.setAdapter(mAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.PLAfab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "OpenUp CompareOrganizeActivity");
                Intent intent = new Intent(activity, CompareOrganizeActivity.class);
                ArrayList<String> photoIds = new ArrayList<>(mData.size());
                ArrayList<String> photoURLs = new ArrayList<>(mData.size());
                ArrayList<String> photoTitles = new ArrayList<>(mData.size());

                for (PhotoItemData data : mData) {
                    photoIds.add(data.id);
                    photoURLs.add(data.thumbnailURL);
                    photoTitles.add(data.text);
                }
                intent.putStringArrayListExtra(Constants.BUNDLE_PHOTOID_LIST_KEY, photoIds);
                intent.putStringArrayListExtra(Constants.BUNDLE_PHOTOURL_LIST_KEY, photoURLs);
                intent.putStringArrayListExtra(Constants.BUNDLE_PHOTOTITLE_LIST_KEY, photoTitles);

                startActivity(intent);
                return;
            }
        });

        mLoadMoreButton = (Button) findViewById(R.id.PLAloadMoreButton);
        mLoadMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FetchPhotosAsyncTask(activity).execute();
            }
        });
        // Restore preferences
        SharedPreferences settings = getSharedPreferences(Constants.PREFS_NAME, 0);

    }

    @Override
    protected void onResume() {
        super.onResume();

        checkAndUpdateLoginStatus();

        // Set photosetId from passed parameters
        Bundle b = getIntent().getExtras();
        if (b != null) {
            String bundlePhotosetId = b.getString(Constants.PHOTOSET_ID_KEY);

            if (bundlePhotosetId == null) {
                Log.v(TAG, "OpenUp PhotoSetListActivity");
                Intent intent = new Intent(this, PhotoSetListActivity.class);
                startActivity(intent);
                return;
            }
            if (!bundlePhotosetId.equals(photosetId)) {
                resetModel();
            }
            photosetId = bundlePhotosetId;
        }
        if (photosetId == null){
            NavUtils.navigateUpFromSameTask(this);
            return;
        }
        if (mData.size() == 0) {
            Activity activity = this;
            new FetchPhotosAsyncTask(activity).execute();
        }
    }

    /**
     * Cleanup state of activity
     */
    private void resetModel() {
        mData.clear();
        mAdapter.clear();
        pageToLoad = 1;
        photosetId = null;
        canLoadMore = true;
    }

    /**
     * Check if logged in, if not launch LoginActivity
     */
    private void checkAndUpdateLoginStatus() {
        if (!FlickrSingleton.getInstance().isLoggedIn()) {
            Log.v(TAG, "OpenUp LoginActivity");
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            return;
        }
    }

    // Called when to add an item to adapter list
    public void addPhotoSetItem(PhotoItemData dataToAdd) {
        mData.add(dataToAdd);
        // Update adapter.
        mAdapter.addItem(mData.size() - 1, dataToAdd);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_photo_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_logout) {
            resetModel();
            FlickrSingleton.getInstance().setAccessToken(null, this);
            checkAndUpdateLoginStatus();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class FetchPhotosAsyncTask extends AsyncTask<URL, Integer, Long> {
        private Activity mActivity;

        FetchPhotosAsyncTask(Activity activity) {
            mActivity = activity;
        }

        protected Long doInBackground(URL... urls) {
            try {
                Flickr f = new Flickr(Constants.apiKey, Constants.sharedSecret, new REST());
                Auth mainAuth = f.getAuthInterface().checkToken(FlickrSingleton.getInstance().getAccessToken().getToken(),
                        FlickrSingleton.getInstance().getAccessToken().getTokenSecret());
                f.setAuth(mainAuth);
                RequestContext.getRequestContext().setAuth(mainAuth);
                PhotosetsInterface photosetsInterface = f.getPhotosetsInterface();

                final PhotoList<Photo> results = photosetsInterface.getPhotos(photosetId, Constants.PHOTO_LIST_PAGE_SIZE, pageToLoad);
                Log.v(TAG, "index=" + results);

                for (Photo p : results) {
                    PhotoItemData data = new PhotoItemData(p.getId(), p.getTitle(), p.getSmall320Url());
                    mData.add(data);
                }
                // Update adapter.
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < results.size(); i++) {
                            mAdapter.addItem(mAdapter.getItemCount(), mData.get(mAdapter.getItemCount()));
                        }
                        mLoadMoreButton.setVisibility(View.VISIBLE);
                    }
                });
                Log.v(TAG, "Adapter data update complete");

                // Change activity state
                pageToLoad = pageToLoad + 1;
            } catch (FlickrException fe) {
                if (fe.getErrorMessage().contains("Photoset not found")) {
                    canLoadMore = false;
                    // Update Load more button.
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mLoadMoreButton.setVisibility(View.INVISIBLE);
                            // Toast user of no more images exist
                            Context context = mActivity.getApplicationContext();
                            CharSequence text = "No more photos to load.";
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                        }
                    });

                }
            } catch (Exception e) {
                Log.v(TAG, "Exception occurred.", e);
            }
            return 0L;
        }

        protected void onProgressUpdate(Integer... progress) {

        }

        protected void onPostExecute(Long result) {

        }
    }
}
