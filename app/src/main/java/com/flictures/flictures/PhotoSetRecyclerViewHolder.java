package com.flictures.flictures;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class PhotoSetRecyclerViewHolder extends RecyclerView.ViewHolder {
    public TextView title;
    public ImageView icon;
    public View view;

    public PhotoSetRecyclerViewHolder(View itemView) {
        super(itemView);
        view = itemView;
        title = (TextView) itemView.findViewById(R.id.PSLItitle);
        icon = (ImageView) itemView.findViewById(R.id.PSLIicon);
    }
}
