package com.flictures.flictures;

public class Constants {

    public static final String PREFS_NAME = "FlicturesPrefsFile";
    public static final String ACCESS_TOKEN_KEY = "accessToken";
    public static final String ACCESS_TOKEN_SECRET_KEY = "accessTokenSecret";

    public static final String apiKey = "ab47abf34304fa764c7050e39b683637";
    public static final String sharedSecret = "ed3074d4eb62ff3c";
    public static final String PROTECTED_RESOURCE_URL = "https://api.flickr.com/services/rest/";
    public static final String PHOTOSET_ID_KEY = "photosetId";

    public static final String BUNDLE_PHOTOID_LIST_KEY = "photoIdsList";
    public static final String BUNDLE_PHOTOTITLE_LIST_KEY = "photoTitlesList";
    public static final String BUNDLE_PHOTOURL_LIST_KEY  = "photoURLsList";
    public static final int PHOTO_LIST_PAGE_SIZE = 20;
    public static final String FLICTURES_CACHE_FOLDER_LOCATION = "/Flictures/cache";
    public static final String BUNDLE_PHOTOLOCAL_FILE_NAME_LIST_KEY = "photoLocalFileNamesList";
}
